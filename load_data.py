import numpy as np
from torch import Tensor
import torch
from torch.utils.data import TensorDataset
import torchvision.transforms as transforms

def load():
    with np.load('dimredux-challenge-01-data.npz') as fh:
        x = fh['data_x']
        val_x = fh['validation_x']
        val_y = fh['validation_y']

    x = Tensor(x)
    val_x = Tensor(val_x)
    val_y = Tensor(val_y)
    
    #print(x.shape)
    #print(val_x.shape)
    #print(val_y.shape)

load()
    